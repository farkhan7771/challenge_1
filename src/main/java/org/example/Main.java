package org.example;

// Farkhan Hamzah Firdaus

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

public class Main {
    static ArrayList<String> namaMenu = new ArrayList<String>();
    static ArrayList<String> jumlahPesan = new ArrayList<String>();
    static ArrayList<String> jumlahUangPesan = new ArrayList<String>();
    static void header() {
        System.out.println("==========================");
        System.out.println("Berapa pesanan anda");
        System.out.println("==========================\n");
    }

    static void headerBinarFud() {
        System.out.println("==========================");
        System.out.println("Selamat datang di BinarFud");
        System.out.println("==========================\n");
    }

    static void jumlahItem() {
        double totalBayar = 0;
        int totalItem = 0;
        DecimalFormat decimalFormat = new DecimalFormat("0.000");
        String newNamaMenu = String.join(",", namaMenu);
        String newJumlahPesan = String.join(",", jumlahPesan);
        String newJumlahUangPesan = String.join(",", jumlahUangPesan);

        String[] newNamaMenuArr = newNamaMenu.split(",");
        String[] newJumlahPesanArr = newJumlahPesan.split(",");
        String[] newJumlahUangPesanArr = newJumlahUangPesan.split(",");

        for (int i = 0; i < newJumlahPesanArr.length; i++) {
            totalBayar += (Double.parseDouble(newJumlahUangPesanArr[i]) * Double.parseDouble(newJumlahPesanArr[i]));
            totalItem += Integer.parseInt(newJumlahPesanArr[i]);
        }

        for (int i = 0; i < newNamaMenuArr.length; i++) {
            System.out.println(newNamaMenuArr[i] + "\t\t" + newJumlahPesanArr[i] + "\t\t" + decimalFormat.format((long) Double.parseDouble(newJumlahUangPesanArr[i]) * Double.parseDouble(newJumlahPesanArr[i]) / 1000));
        }
        System.out.println("-------------------------------+");
        System.out.println("Total\t\t\t" + totalItem + "\t\t" + decimalFormat.format((double) totalBayar / 1000 ));
    }

    static void orderMenu(int pilihMenu) {
        Scanner input = new Scanner(System.in);
        if (pilihMenu == 1) {
            System.out.println("Nasi Goreng\t|\t15.000");
            System.out.println("(input 0 untuk kembali)");
            System.out.print("\nqty => ");
            String jumlahPesanan = input.nextLine();
            if (Objects.equals(jumlahPesanan, "0")) {
                home();
            }
            if (!namaMenu.contains("Nasi Goreng")) {
                namaMenu.add("Nasi Goreng");
                jumlahPesan.add(jumlahPesanan);
                jumlahUangPesan.add("15000");
            } else {
                int indexItem = namaMenu.indexOf("Nasi Goreng");
                int jumPesan = Integer.parseInt(jumlahPesanan) + Integer.parseInt(jumlahPesan.get(indexItem));
                jumlahPesan.set(indexItem, Integer.toString(jumPesan));
            }
        } else if (pilihMenu == 2) {
            System.out.println("Mie Goreng\t|\t13.000");
            System.out.println("(input 0 untuk kembali)");
            System.out.print("\nqty => ");
            String jumlahPesanan = input.nextLine();
            if (Objects.equals(jumlahPesanan, "0")) {
                home();
            }
            if (!namaMenu.contains("Mie Goreng")) {
                namaMenu.add("Mie Goreng");
                jumlahPesan.add(jumlahPesanan);
                jumlahUangPesan.add("13000");
            } else {
                int indexItem = namaMenu.indexOf("Mie Goreng");
                int jumPesan = Integer.parseInt(jumlahPesanan) + Integer.parseInt(jumlahPesan.get(indexItem));
                jumlahPesan.set(indexItem, Integer.toString(jumPesan));
            }
        } else if (pilihMenu == 3) {
            System.out.println("Nasi + Ayam\t|\t18.000");
            System.out.println("(input 0 untuk kembali)");
            System.out.print("\nqty => ");
            String jumlahPesanan = input.nextLine();
            if (Objects.equals(jumlahPesanan, "0")) {
                home();
            }
            if (!namaMenu.contains("Nasi + Ayam")) {
                namaMenu.add("Nasi + Ayam");
                jumlahPesan.add(jumlahPesanan);
                jumlahUangPesan.add("18000");
            } else {
                int indexItem = namaMenu.indexOf("Nasi + Ayam");
                int jumPesan = Integer.parseInt(jumlahPesanan) + Integer.parseInt(jumlahPesan.get(indexItem));
                jumlahPesan.set(indexItem, Integer.toString(jumPesan));
            }
        } else if (pilihMenu == 4) {
            System.out.println("Es Teh Manis\t|\t3.000");
            System.out.println("(input 0 untuk kembali)");
            System.out.print("\nqty => ");
            String jumlahPesanan = input.nextLine();
            if (Objects.equals(jumlahPesanan, "0")) {
                home();
            }
            if (!namaMenu.contains("Es Teh Manis")) {
                namaMenu.add("Es Teh Manis");
                jumlahPesan.add(jumlahPesanan);
                jumlahUangPesan.add("3000");
            } else {
                int indexItem = namaMenu.indexOf("Es Teh Manis");
                int jumPesan = Integer.parseInt(jumlahPesanan) + Integer.parseInt(jumlahPesan.get(indexItem));
                jumlahPesan.set(indexItem, Integer.toString(jumPesan));
            }
        } else if (pilihMenu == 5) {
            System.out.println("Es Jeruk\t|\t5.000");
            System.out.println("(input 0 untuk kembali)");
            System.out.print("\nqty => ");
            String jumlahPesanan = input.nextLine();
            if (Objects.equals(jumlahPesanan, "0")) {
                home();
            }
            if (!namaMenu.contains("Es Jeruk")) {
                namaMenu.add("Es Jeruk");
                jumlahPesan.add(jumlahPesanan);
                jumlahUangPesan.add("5000");
            } else {
                int indexItem = namaMenu.indexOf("Es Jeruk");
                int jumPesan = Integer.parseInt(jumlahPesanan) + Integer.parseInt(jumlahPesan.get(indexItem));
                jumlahPesan.set(indexItem, Integer.toString(jumPesan));
            }
        } else if (pilihMenu == 0) {
            System.exit(0);
        } else {
            home();
        }
    }

    static void konfirmasiOrder() {
        Scanner input = new Scanner(System.in);
        System.out.println("==========================");
        System.out.println("Konfirmasi dan Pembayaran");
        System.out.println("==========================\n");

        jumlahItem();

        System.out.println("\n1. Konfirmasi dan Bayar\n2. Kembali ke menu utama\n0. Keluar aplikasi");
        System.out.print("\n=> ");
        int pilihMenu2 = input.nextInt();
        if (pilihMenu2 == 1) {
            strukPembayaran();
        } else if (pilihMenu2 == 2) {
            home();
        } else if (pilihMenu2 == 0) {
            System.out.println("Keluar aplikasi");
        } else {
            home();
        }
    }

    static void strukPembayaran() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("berekstensi.txt"))) {
            writer.newLine();
            writer.write("==========================");
            writer.newLine();
            writer.write("BinarFud");
            writer.newLine();
            writer.write("==========================");
            writer.newLine();
            writer.newLine();

            writer.write("Terima kasih sudah memesan\ndi BinarFud");
            writer.newLine();
            writer.write("\nDi bawah ini adalah pesanan anda\n");
            writer.newLine();

            double totalBayar = 0;
            int totalItem = 0;
            DecimalFormat decimalFormat = new DecimalFormat("0.000");
            String newNamaMenu = String.join(",", namaMenu);
            String newJumlahPesan = String.join(",", jumlahPesan);
            String newJumlahUangPesan = String.join(",", jumlahUangPesan);

            String[] newNamaMenuArr = newNamaMenu.split(",");
            String[] newJumlahPesanArr = newJumlahPesan.split(",");
            String[] newJumlahUangPesanArr = newJumlahUangPesan.split(",");

            for (int i = 0; i < newJumlahPesanArr.length; i++) {
                totalBayar += (Double.parseDouble(newJumlahUangPesanArr[i]) * Double.parseDouble(newJumlahPesanArr[i]));
                totalItem += Integer.parseInt(newJumlahPesanArr[i]);
            }

            for (int i = 0; i < newNamaMenuArr.length; i++) {
                writer.write(newNamaMenuArr[i] + "\t\t" + newJumlahPesanArr[i] + "\t\t" + (decimalFormat.format((long) Double.parseDouble(newJumlahUangPesanArr[i]) * Double.parseDouble(newJumlahPesanArr[i]) / 1000)) + "\n");
            }

            String res = "Total\t\t\t" + totalItem + "\t\t" + decimalFormat.format((double) totalBayar / 1000 );

            writer.write("-------------------------------+");
            writer.newLine();
            writer.write(res);
            writer.newLine();

            writer.write("\nPembayaran : BinarCash\n");
            writer.newLine();
            writer.write("==========================");
            writer.newLine();
            writer.write("Simpan struk ini sebagai\nbukti pembayaran");
            writer.newLine();
            writer.write("==========================");
            writer.newLine();

            System.out.println("\nStruk berhasil disimpan.");
        } catch (IOException e) {
            System.out.println("Gagal menyimpan struk: " + e.getMessage());
        }
    }
    static void home() {
        Scanner input = new Scanner(System.in);

        headerBinarFud();

        System.out.println("Silahkan pilih makanan :");
        System.out.println("1. Nasi Goreng\t|\t15.000\n2. Mie Goreng\t|\t13.000\n3. Nasi + Ayam\t|\t18.000\n" +
                "4. Es Teh Manis\t|\t3.000\n5. Es Jeruk\t\t|\t5.000\n" +
                "99. Pesan dan Bayar\n0. Keluar aplikasi");
        System.out.print("\n=> ");
        int pilihMenu = input.nextInt();

        if (pilihMenu == 1) {
            header();
            orderMenu(pilihMenu);
            home();
        } else if (pilihMenu == 2) {
            header();
            orderMenu(pilihMenu);
            home();
        } else if (pilihMenu == 3) {
            header();
            orderMenu(pilihMenu);
            home();
        } else if (pilihMenu == 4) {
            header();
            orderMenu(pilihMenu);
            home();
        } else if (pilihMenu == 5) {
            header();
            orderMenu(pilihMenu);
            home();
        } else if (pilihMenu == 99) {
            konfirmasiOrder();
        } else if (pilihMenu == 0) {
            System.exit(0);
        } else {
            home();
        }
    }
    public static void main(String[] args) {
        home();
    }
}